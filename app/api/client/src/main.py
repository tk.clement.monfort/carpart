from fastapi import FastAPI
import mysql.connector

app = FastAPI()

def connect():
    return (mysql.connector.connect(host='192.168.42.87', port=22002, user='admin', password='secret', database='Client'))

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this fantastic app!"}

@app.get("/client/{name}")
async def getClientFromName(name = str):
    conn = connect()
    cursor = conn.cursor()
    query = "SELECT * FROM Client WHERE name LIKE '%{$" + name + "}%'"
    cursor.execute(query)
    var = list(cursor.fetchall())
    conn.commit()
    conn.close()

    return (var)

@app.get("/client/")
async def getClient():
    conn = connect()
    cursor = conn.cursor()
    query = "SELECT * FROM Client"
    cursor.execute(query)
    var = list(cursor.fetchall())
    conn.commit()
    conn.close()

    return (var)

@app.put("/client/")
async def addClient(name = str, surname = str, mail = str):
    conn = connect()
    cursor = conn.cursor()
    query = 'INSERT INTO Client (name, surname, mail) VALUES (%s, %s, %s)'
    values = (name, surname, mail)
    cursor.execute(query, values)
    conn.commit()
    conn.close()

