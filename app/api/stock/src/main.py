from fastapi import FastAPI
from pymongo import MongoClient

client = MongoClient('mongodb://admin:secret@192.168.42.202:22000')
col = client['Itemdb']['Item']
app = FastAPI()

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this fantastic app!"}

@app.get("/item/")
async def getItemList():
    return (list(col.find({}, {'_id': 0})))

@app.put("/item/")
async def addItem(name = str, brand = str, desc = str, stock = int):
    col.insert_one({'name': name, 'brand': brand, 'desc': desc, 'stock': stock})

@app.get("/item/{name}")
async def getItem(name = str):
    return (list(col.find({'name': name}, {'_id': 0})))

@app.put("/item/stock/")
async def setStock(name = str, stock = int):
    col.update_one({'name': name}, {'stock': stock})

@app.put("/item/desc/")
async def setDesc(name = str, desc = str):
    col.update_one({'name': name}, {'desc': desc})

@app.put("/item/rm/")
async def rmItem(name = str):
    col.delete_one({'name': name})

