CARD=$(ip route | grep -m1 default | cut -d ' ' -f5)
WAN=$(ip route | grep $CARD | tail +2 | cut -d ' ' -f9)

echo "modify the main.py file to use $WAN instead"

sudo docker rm -f api db

cd app/mongo/ && docker build -t apimongo . -f Dockerfile
cd ../api/ && docker build -t apipython . -f Dockerfile

docker run -p $WAN:21000:27017/tcp -d --restart unless-stopped --name db apimongo
docker run -p $WAN:21001:80/tcp -d --restart unless-stopped --name api apipython
#docker compose down && docker system prune -fa --volumes && docker compose up -d